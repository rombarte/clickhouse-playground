<?php

require_once "vendor/autoload.php";

$db = new ClickHouseDB\Client(
    [
        'host' => 'clickhouse',
        'port' => '8123',
        'username' => 'username',
        'password' => 'password',
    ]
);
$db->database('default');
$db->ping();
